#pragma once
#include "Customer.h"
Customer::Customer(string name )
{
	_name = name;
}
Customer::~Customer()
{

}
double Customer::totalSum() const
{
	int count=0;
	for (Item item: _items)
	{
		count += item.totalPrice();
	}
	return count;
}
void Customer::addItem(Item a)
{
	int counter = 1;
	if (_items.find(a) != _items.end())
	{
		counter =  _items.find(a)->getCount()+1;
		
		_items.erase(a);
		
	}
	a.setCount(counter);
	_items.insert(a);
	
}
void Customer::removeItem(Item a)
{
	int counter = 1;
	if (_items.find(a) != _items.end())
	{
		counter = _items.find(a)->getCount() - 1;

		_items.erase(a);

	}
	if (counter > 0)
	{
		a.setCount(counter);
		_items.insert(a);
	}
}
string Customer::getName()
{
	return _name;
}
void Customer::setName(string name)
{
	_name = name;
}
int Customer::getItems()
{
	int count = 0;
	for (Item item : _items)
	{
		count++;
		std::cout << item.getName() <<" "<<item.getCount()<< std::endl;
	}
	return count;
}