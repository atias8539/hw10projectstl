#pragma once
#include "Item.h"

Item::Item(string name, string serinum, double unitprice)
{
	_name = name;
	_serialNumber = serinum;
	_count = 1;
	_unitPrice = unitprice;
}

Item::~Item()
{

}

double Item::totalPrice() const
{
	return(_count*_unitPrice);
}
bool Item::operator <(const Item& other) const
{
	return(_serialNumber < other._serialNumber);
}
bool Item::operator >(const Item& other) const
{
	return(_serialNumber > other._serialNumber);
}
bool Item::operator ==(const Item& other) const
{
	return(_serialNumber == other._serialNumber);
}

string Item::getName()
{
	return (_name);
}
string Item::getSerialNumber()
{
	return(_serialNumber);
}
void Item::setName(string name)
{
	_name = name;
}
void Item::setSerialNumbers(string serialNUMber)
{
	_serialNumber = serialNUMber;
}
double Item::getUnitPrice()
{
	return _unitPrice;
}
int Item::getCount() const
{
	return _count;
}
void Item::setCount(int c)
{
	_count = c;
}
